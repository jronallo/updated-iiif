var lunr = require('lunr'),
    stdout = process.stdout,
    fs = require('fs')

require("lunr-languages/lunr.stemmer.support")(lunr)
require("lunr-languages/lunr.multi")(lunr)

fs.readFile('data.json', 'utf8', function(err, data){
  var documents = JSON.parse(data),
      docs = {}

  var idx = lunr(function () {
    this.use(lunr.multiLanguage('en'))
    this.ref('id')
    this.field('title')
    // this.field('body')

    documents.forEach(function (doc) {
      this.add(doc)
    }, this)
  })

  documents.forEach(function (doc) {
    docs[doc.id] = {
      title: doc.title
    }
  })

  json_index = JSON.stringify(idx)
  index = JSON.parse(json_index)
  index['docs'] = docs

  stdout.write(JSON.stringify(index))
})
