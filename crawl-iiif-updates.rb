#!/usr/bin/env ruby

require 'erb'
require 'json'
require 'httpclient'
require 'date'
client = HTTPClient.new
index_docs = []
times = 10

more_cdm = [
  'http://52.204.112.237:3051/activity-streams/15878',
  'http://52.204.112.237:3051/activity-streams/16003',
  'http://52.204.112.237:3051/activity-streams/16007',
  'http://52.204.112.237:3051/activity-streams/16022',
  'http://52.204.112.237:3051/activity-streams/16214',
  'http://52.204.112.237:3051/activity-streams/17272'
]

urls = [
  # 'http://52.204.112.237:3051/activity-streams/17287',
  'https://mcgrattan.org/as/',
  'https://d.lib.ncsu.edu/collections/iiif-discovery.json'
]

urls.each do |url|
  collection_json = client.get_content(url)
  collection = JSON.parse(collection_json)
  page_id = collection['last']['id']
  page_json = client.get_content(page_id)
  page = JSON.parse(page_json)
  items = page['orderedItems']
  times.times do
    item = items.pop
    object = item['object']
    manifest_id = object['id']
    puts manifest_id
    manifest_json = client.get_content(manifest_id)
    manifest = JSON.parse(manifest_json)
    doc = {
      manifest: manifest_id,
      title: manifest['label'],
      updated: item['endTime'],
      page: page_id
    }
    index_docs << doc
  end
end

index_docs.sort_by do |doc|
  doc['endTime']
end

index_docs.uniq do |doc|
  doc['manifest']
end

index_docs.map.with_index do |doc, index|
  doc[:id] = index
  doc
end

puts index_docs

File.open('data.json', 'w') do |fh|
  fh.puts JSON.dump index_docs
end

template = File.read('updated.html.erb')
renderer = ERB.new(template)
File.open('updated.html', 'w') do |fh|
  fh.puts renderer.result()
end

`node build-index-from-file.js > index.json`

%w[
  updated.html
  search.js
  index.json
].each do |file|
  `rsync -avzc #{file} ronallo:/var/www/demos/iiif-discovery/.`
end
