# Updated IIIF

Simple demo of crawling some activity streams for just the most recently updated IIIF resources and displaying them on a page. Allows for filtering results with a search on titles powered by [lunr.js](https://github.com/olivernn/lunr.js). Intended to be an initial example of simple crawling for creating a client-side search index.

[Example](http://ronallo.com/demos/iiif-discovery/updated.html)

## Development

Requires:
```
npm i --global lunr lunr-languages
```

Run the demo with:
```sh
ruby crawl-iiif-updates.rb
```
