$(document).ready(function() {

  $.getJSON('./index.json', function(response) {

    // Create index
    index = lunr.Index.load(response);

    // Handle search
    $('input#search').on('keyup', function() {
      $('.doc').hide();

      var query = $(this).val();

      if (query.length ===0) {
        $('.doc').show();
      } else if (query.length > 2) {
        var result = index.search(query + '*');
        if (result.length === 0) {
          console.log('no results')
        } else {
          for (var item in result) {
            var ref = result[item].ref
            console.log(ref)
            $('#id' + ref).show();
          }
        }

      }

    });

  });
});
